# Currencies Ticker

Display daily USD exchange rate for different currencies and percentage change since the last day.  
Sort by currency name, exchange rate and percentage change.

## Getting Started

* Clone repo `git clone git@gitlab.com:barro32/currencies-ticker.git`
* `cd currencies-ticker`
* Install Create React App `npm install -g create-react-app`
* Run dev build `npm start`

## Built With

* [Create React App](https://github.com/facebookincubator/create-react-app)

## Author

* **Daniel Barrington**