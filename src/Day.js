import React, { Component } from 'react';
import './Day.css'
import Currency from './Currency';

class Day extends Component {

	constructor(props) {
		super(props);
		this.state = {
			rates: this.props.day.rates,
			sortAlpha: true,
			sortRate: false,
			sortChange: false,
			selectAlpha: true,
			selectRate: false,
			selectChange: false
		}
		this.sortCode = this.sortCode.bind(this);
		this.sortRate = this.sortRate.bind(this);
		this.sortChange = this.sortChange.bind(this);
	}

	sortCode() {
		if(this.state.sortAlpha) {
			this.setState({
				rates: [].concat(this.state.rates.sort((a, b) => a.code < b.code)),
				sortAlpha: false,
				sortRate: false,
				sortChange: false
			});
		} else {
			this.setState({
				rates: [].concat(this.state.rates.sort((a, b) => a.code > b.code)),
				sortAlpha: true,
				sortRate: false,
				sortChange: false
			});
		}
		this.setState({
			selectAlpha: true,
			selectRate: false,
			selectChange: false
		});
	}

	sortRate() {
		if(this.state.sortRate) {
			this.setState({
				rates: [].concat(this.state.rates.sort((a, b) => Number(a.rate) > Number(b.rate))),
				sortAlpha: false,
				sortRate: false,
				sortChange: false
			});
		} else {
			this.setState({
				rates: [].concat(this.state.rates.sort((a, b) => Number(a.rate) < Number(b.rate))),
				sortAlpha: false,
				sortRate: true,
				sortChange: false
			});
		}
		this.setState({
			selectAlpha: false,
			selectRate: true,
			selectChange: false
		});
	}

	sortChange() {
		if(this.state.sortChange) {
			this.setState({
				rates: [].concat(this.state.rates.sort((a, b) => Number(a.change) > Number(b.change))),
				sortAlpha: false,
				sortRate: false,
				sortChange: false
			});
		} else {
			this.setState({
				rates: [].concat(this.state.rates.sort((a, b) => Number(a.change) < Number(b.change))),
				sortAlpha: false,
				sortRate: false,
				sortChange: true
			});
		}
		this.setState({
			selectAlpha: false,
			selectRate: false,
			selectChange: true
		});
	}

	render() {
		return (
			<div className="Day">
				<h3>{this.props.timestamp}</h3>
				<div className="sort-buttons">
					<button onClick={this.sortCode} className={"sort-code " + (this.state.selectAlpha ? 'selected' : '')}>A-Z</button>
					<button onClick={this.sortRate} className={"sort-rate " + (this.state.selectRate ? 'selected' : '')}>Rate</button>
					<button onClick={this.sortChange} className={"sort-change " + (this.state.selectChange ? 'selected' : '')}>%</button>
				</div>
				{this.state.rates.map((currency) => 
					<Currency code={currency.code} rate={currency.rate} change={currency.change} key={currency.code} />
				)}
			</div>
		);
	}
}

export default Day