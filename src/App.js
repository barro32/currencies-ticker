import React, { Component } from 'react';
import './App.css'
import Day from './Day';
import currencies from './currencies.json';

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			currencies: []
		};

		this.formatData();
	}

	formatData() {
		let currencyArray = [];

		// convert Objects to Arrays
		Object.keys(currencies).forEach(function(day) {
			let date = new Date (currencies[day].timestamp * 1000);
			let base = currencies[day].base;
			let rates = [];
			Object.keys(currencies[day].rates).forEach(function(code) {
				rates.push({code: code, rate: currencies[day].rates[code]});
			});
			currencyArray.push({timestamp: date, base: base, rates: rates});
		});

		// Sort by date
		currencyArray.sort((a, b) => a.timestamp > b.timestamp);
		
		// add rate change data
		let prev = null;
		for(let i in currencyArray) {
			if(prev === null) {
				prev = currencyArray[i];
			} else {
				for(let j in currencyArray[i].rates) {
					let todaysRate = currencyArray[i].rates[j].rate;
					let yesterdaysRate = prev.rates[j].rate;
					let change = ((todaysRate - yesterdaysRate) / yesterdaysRate) * 100;
					currencyArray[i].rates[j].change = change.toFixed(2);
				}
				prev = currencyArray[i];
			}
		};

		this.state.currencies = currencyArray;
	}

	render() {
		return (
			<div className="App">
				<div className="App-header">
					<h2>Currency Ticker</h2>
					<h3>USD rates</h3>
				</div>
				<div className="scroll-window">
					<div className="ticker">
						{this.state.currencies.map((day) =>
							<Day day={day} timestamp={day.timestamp.toLocaleDateString()} key={day.timestamp.toLocaleDateString()} />
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default App;
