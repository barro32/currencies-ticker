import React, { Component } from 'react';
import './Currency.css'

class Currency extends Component {
	render() {
		return (
			<div className="Currency">
				<span className="code">{this.props.code}</span>
				<span className="rate">{this.props.rate}</span>
				{this.props.change ? <span className={"change " + (this.props.change > 0 ? 'green' : 'red')}>{this.props.change}</span> : null}
			</div>
		);
	}
}

export default Currency